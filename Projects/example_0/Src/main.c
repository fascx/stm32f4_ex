/**
 ******************************************************************************
 * @file    main.c 
 * @author  Fredy Alexander Suárez C
 * @version V1.0
 * @date    06-Junio-2014
 * @brief   This example describes how to configure and use GPIOs at STM32F4xx.
 ******************************************************************************
 * @attention
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define GPIOPIN_5  5
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_TypeDef* GPIO_A = GPIOA;
/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {

    /* STM32F4xx HAL library initialization:
         - Configure the Flash prefetch, instruction and Data caches
         - Configure the Systick to generate an interrupt each 1 msec
         - Set NVIC Group Priority to 4
         - Global MSP (MCU Support Package) initialization
     */

    FLASH->ACR |= FLASH_ACR_ICEN;

    FLASH->ACR |= FLASH_ACR_DCEN;

    FLASH->ACR |= FLASH_ACR_PRFTEN;
    
    SysTick_Config(HSI_VALUE/ 1000); //1 msec
    
    NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    /* Configure the system clock */
    SystemClock_Config();

/* -1- Enable GPIOA Clock (to be able to program the configuration registers) */
    RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN);

/* -2- Configure PA05 IO in output push-pull mode to*/
    
    uint32_t GPIOBIT = ((uint32_t)0x01) << GPIOPIN_5;
    
/*------------------------- GPIO Mode Configuration --------------------*/
/*----------------------------------------------------------------------*/	
	/* Configure the default Alternate Function in current IO */
	GPIO_A->AFR[GPIOPIN_5 >> 3] &= ~((uint32_t)0xF << ((uint32_t)(GPIOPIN_5 & (uint32_t)0x07) * 4)) ;

    /*----IO Speed */
	//00b: Low speed
	//01b: Medium speed
	//10b: Fast speed
	//11b: High speed
	/* Configure the default value for IO Speed */
	GPIO_A->OSPEEDR &= ~((uint32_t)0x03 << (GPIOPIN_5 * 2));
	/* Configure the IO Speed */
	GPIO_A->OSPEEDR |= ((uint32_t)0x02 << (GPIOPIN_5 * 2));
/*----------------------------------------------------------------------*/	
	/*----IO Output Type */
	//0b: Output push-pull (reset state)
	//1b: Output open-drain
	/* Configure the default value IO Output Type */
	GPIO_A->OTYPER  &= ~((uint32_t)0x0 << GPIOPIN_5) ;
	/* Configure the IO Output Type */
	GPIO_A->OTYPER |= ((uint32_t)0x0) << GPIOPIN_5;
/*----------------------------------------------------------------------*/		
	/*----IO Direction mode*/	
	//00b: Input (reset state)
	//01b: General purpose output mode
	//10b: Alternate function mode
	//11b: Analog mode
	
    /* Configure IO Direction in Input Floting Mode */
    GPIO_A->MODER &= ~((uint32_t)0x0 << (GPIOPIN_5 * 2));
    /* Configure IO Direction mode*/
    GPIO_A->MODER |= ((uint32_t)0x1) << (GPIOPIN_5 * 2);  
/*----------------------------------------------------------------------*/	    
	/*----Pull-up or Pull down resistor for the current IO */
	//00b: No pull-up, pull-down
	//01b: Pull-up
	//10b: Pull-down
	//11b: Reserved
	/* Deactivate the Pull-up oand Pull-down resistor for the current IO */
	GPIO_A->PUPDR &= ~(GPIO_PUPDR_PUPDR0 << (GPIOPIN_5 * 2));
	/* Activate the Pull-up or Pull down resistor for the current IO */
	GPIO_A->PUPDR |= ((GPIO_PULLUP) << (GPIOPIN_5 * 2));
/*----------------------------------------------------------------------*/	

/* -3- Toggle PA05 IO in an infinite loop */
    while (1) {
        GPIO_A->ODR ^=GPIOBIT;//GPIO_PIN_5;

        /* Insert delay 100 ms */
        HAL_Delay(500);
    }
}







/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow : 
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 84000000
 *            HCLK(Hz)                       = 84000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 2
 *            APB2 Prescaler                 = 1
 *            HSI Frequency(Hz)              = 16000000
 *            PLL_M                          = 16
 *            PLL_N                          = 336
 *            PLL_P                          = 4
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale2 mode
 *            Flash Latency(WS)              = 2
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void) {
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable Power Control clock */
    __PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is 
       clocked below the maximum system frequency, to update the voltage scaling value 
       regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /* Enable HSE Oscillator and activate PLL with HSI as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 16;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
       clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
        Error_Handler();
    }
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */

static void Error_Handler(void) {
    while (1) {
    }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line) {
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1) {
    }
}
#endif
